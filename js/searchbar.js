$(document).ready(function(){
	$('#searchbar').keyup(function(){
		var search = $(this).val();
		search = $.trim(search);
		$.post("ajax/searchbar.php",{search:search},function(data){
	        $('#films ul').html(data);
	    });
	});
});